import {createStore, combineReducers, compose} from 'redux'
import * as reducers from './reducers'
import createLogger from 'redux-logger'


const reducer = combineReducers(reducers);

const store = createStore(reducer,{
    posts: [],
    username: {
        name: null,
        authorization: false
    }

});
export default store;
/**
 * Created by Artsiom_Rakitski on 2/25/2016.
 */