import React from 'react'
import ReactDom from 'react-dom'
import {Provider} from 'react-redux'
import App from './app/App'
import store from './state'
import {addPost} from './actions'

ReactDom.render(
    <Provider store={store}>
        <App/>
    </Provider>,
    document.getElementById('root')
);
//ReactDom.render(
//    <App store={store}/>,
//    document.getElementById('root')
//);
store.subscribe(() => console.log('выведись ',store.getState()));
//
//store.dispatch({
//    type: 'INCREASE_COUNTER'
//});
//console.log(3);
//store.dispatch({
//    type: 'RESET_COUNTER'
//});
//store.dispatch({
//    type: 'INCREASE_COUNTER'
//});
//store.dispatch({
//    type: 'INCREASE_COUNTER'
//});

