const  middleware = store => next => action =>{
    if (action.type !== 'LOAD_FROM_SERVER'){
        return next(action)
    }
    const [startLoad, successLoad, failLoad]= action.actions;
    store.dispatch({
        type: startLoad
    });
    action.promise.then((data)=>store.dispatch({
        type: successLoad,
        data
    }),(error)=> store.dispatch({
        type: failLoad,
        error
    }))
};
export default middleware;
/**
 * Created by Artsiom_Rakitski on 14/3/2016.
 */
