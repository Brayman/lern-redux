export function addPost(postData){
    return{
        type: 'ADD_POST',
        postData
    }
}
export function sighIn(name){
    return{
        type: 'LOG_IN',
        name,
        authorization: true
    }
}
export function sighOut(){
    return{
        type: 'LOG_OUT',
        authorization: false
    }
}
export function getPosts(){
    $.ajax({
        url: '../src/data',
        dataType: 'json',
        cache: false,
        success: function(data) {
            return({data: data});
        }.bind(this),
        error: function(xhr, status, err) {
            console.error("/../src/data", status, err.toString());
        }.bind(this)
    });
};
/**
 * Created by Artsiom_Rakitski on 2/26/2016.
 */
