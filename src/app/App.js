import React from 'react'
import {connect } from 'react-redux'
import {addPost, sighIn, sighOut, getPosts} from '../actions'
import Authorization from './login'
var Header = React.createClass({
    componentDidMount(){
        console.log(getPosts())
    },
    render: function(){
        return (
            <div className="ui teal inverted segment" id="header">
                <h1>
                    {this.props.name}
                </h1>
                <Authorization/>
            </div>
        )
    }
});
    var App = React.createClass({

    render: function() {
        return (
            <div>
                <Header name={this.props.name}/>
                <PostList posts={this.props.posts}/>
                <Editor counterr={this.props.posts.length}
                        name={this.props.name}
                        onAddClick={text => this.props.dispatch(addPost(text))}
                />
            </div>
        );
    }
});

var Post = React.createClass({
    render: function() {
        return (
            <div className="ui segment" id="post">
                <h1>{this.props.data.name}</h1>
                <p>{this.props.data.text}</p>
            </div>
        );
    }
});
var LogIn = React.createClass({
    render: function() {
        return (
            <div className="ui form">
                <div className="two fields">
                    <div className="field">
                        <input type="text"
                               placeholder="User Name"
                               ref="username"/>
                    </div>
                    <div className="field">
                        <button className="ui button" onClick={this.LogIn}>
                            log in
                        </button>
                    </div>
                </div>
            </div>
        );
    }
});
var PostList = React.createClass({
    componentDidMount(){

    },
    render: function() {
        var posts=this.props.posts;
        return (
        <div>
            {posts.map(function(post){
                return (
                    <Post key={post.id} data={post}/>
                )
            })}
        </div>
        );
    }
});
var Editor = React.createClass({
    TextPost: function(e) {
        this.setState({text: e.target.value})
    },
    SendClick: function() {
        if (this.props.name!='') {
            this.props.onAddClick({
                id: this.props.counterr,
                name: this.props.name,
                text: this.state.text
            });
        }
    },
    render: function() {
        return (
            <div className="ui form" id="post">
                <div className="field">
                    <label>Text</label>
                        <textarea rows="3"
                                  onChange={this.TextPost}/>
                    <button className="ui fluid teal bottom attached button" onClick={this.SendClick}>Send</button>
                </div>
            </div>
        );
    }
});
export default connect(
    (state)=> {return{
        posts: state.posts,
        name: state.username.name
        //valid: state.valid
    }})(App)
/**
 * Created by Artsiom_Rakitski on 2/26/2016.
 */
