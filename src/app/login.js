import React from 'react'
import {connect } from 'react-redux'
import {sighIn, sighOut} from '../actions'

var Authorization = React.createClass({
    render: function() {
        var login=this.props.authorization==false ? <Login login={name=> this.props.dispatch(sighIn(name))}/> : <Logout logout={()=> this.props.dispatch(sighOut())} />;
        return (
            <div>
                {login}
            </div>
        );
    }
});
var Login = React.createClass({
    LogIn: function(){
        let user = this.refs.username.value;
        this.props.login(user);
        //if(user!==''){
        //
        //}
        //else {
        //    user = this.refs.firstname.value + ' '+ this.refs.lastname.value;
        //    this.props.login(user)
        //}
    },
    render: function() {
        return (
            <div >
                <div className="ui input" ><input type="text"
                       placeholder="User Name"
                       ref="username"/></div>
                <button className="ui button"
                        onClick={this.LogIn}>
                    log in
                </button>
            </div>
        );
    }
});
var Logout = React.createClass({
    LogOut: function(){
        this.props.logout();
    },
    render: function() {
        return (
            <button className="ui button"
                    onClick={this.LogOut}>
                log out
            </button>
        );
    }
});
export default connect(
    (state)=> {return{
        name: state.username,
        authorization: state.username.authorization
    }})(Authorization)
/**
 * Created by xolod on 13.03.2016.
 */
