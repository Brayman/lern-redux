export default function (state=[],action){
    switch (action.type){
        case 'ADD_POST':
            return [...state, action.postData];
        default:
            return state;
    }
}
/**
 * Created by Artsiom_Rakitski on 2/25/2016.
 */
