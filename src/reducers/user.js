/**
 * Created by Artsiom_Rakitski on 3/1/2016.
 */
export default function (state='',action){
    switch (action.type){
        case 'LOG_IN':
            return {name: action.name, authorization: true};
        case 'LOG_OUT':
            return {name: null, authorization: false};
        default:
            return state;
    }
}